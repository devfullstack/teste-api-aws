aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 930510614491.dkr.ecr.us-east-1.amazonaws.com
docker build -t teste-api .
docker tag teste-api:latest 930510614491.dkr.ecr.us-east-1.amazonaws.com/teste-api:latest
docker push 930510614491.dkr.ecr.us-east-1.amazonaws.com/teste-api:latest
docker rmi -f $(docker images -q 930510614491.dkr.ecr.us-east-1.amazonaws.com/teste-api:latest)